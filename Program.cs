﻿using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using System.Text.RegularExpressions;

if (Environment.GetCommandLineArgs().Length < 2)
{
    return;
}

for (int arg = 1; arg < Environment.GetCommandLineArgs().Length; arg++)
{
    if (!File.Exists(Environment.GetCommandLineArgs()[arg]))
    {
        return;
    }

    PdfDocument pdfDoc = new(new PdfReader(Environment.GetCommandLineArgs()[arg]));

    List<string> rows = new();

    for (int pageNo = 1; pageNo <= pdfDoc.GetNumberOfPages(); pageNo++)
    {
        SimpleTextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

        PdfPage page = pdfDoc.GetPage(pageNo);
        PdfCanvasProcessor parser = new PdfCanvasProcessor(strategy);
        parser.ProcessPageContent(page);
        var content = strategy.GetResultantText();
        var matches = Regex.Matches(content, @"(\d\d\.\d\d.\d{4}) (.*) (-?[\d\.]*,\d\d)\n(?:\d\n)?(\d\d\.\d\d.\d{4})((?:(?!\d\d\.\d\d\.\d{4})(?:.|\s))*)");
        foreach (Match match in matches)
        {
            rows.Add($"{match.Groups[1]}\t{match.Groups[4]}\t{match.Groups[3]}\t{match.Groups[2]}\t\"{match.Groups[5].Value.Replace("\"", "''").Trim()}\"");
        }
    }
    File.WriteAllLines(Environment.GetCommandLineArgs()[arg] + ".csv", rows.ToArray());
    File.AppendAllLines("all.csv", rows.ToArray());
    pdfDoc.Close();
}

